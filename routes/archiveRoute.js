const express = require("express");

// Creates a router instance that functions as a middleware and routing system
const router = express.Router();

// Route for checking if the user's email already exists in the database
const archiveController = require("../controllers/archiveController");

const auth = require("../auth");