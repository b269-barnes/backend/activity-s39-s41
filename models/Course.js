// s37 Activity

const mongoose = require("mongoose");
const courseSchema = new mongoose.Schema({

	name : {
		type : String,
		required : [true , "Course is required"],
	},
	description : {
		type : String,
		required : [true, "Description is required"]
	},
	price : {
		type : Number,
		required : [true , "Price is required"]
	},
	isActive : {
		type : Boolean,
		dafault :  true
	}, 
	createdOn : {
		type : Date,
		dafault : new Date()
	},
	enrollees : [
	{
		userID : {
			type : String,
			required : [true, "UserID is required"]
		},
		enrolledOn : {
			type : Date,
			default : new Date()
		}
	}
  ]
});

module.exports = mongoose.model("course", courseSchema)