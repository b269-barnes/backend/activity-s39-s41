const Archive = require("./models/Archive");

const bcrypt = require("bcrypt");

const arch = require("../arch");

module.exports.addArchive = (data) => {
	if (data.isActive) {
		let newArchive = new Archive({
			name : data.archive.name,
			isActive : data.archive.isActive
		});

		return newArchive.save().then((archive,error) =>{
			if (error) {
				return false
			} else {
				return true;
			}
		})
	}
};
